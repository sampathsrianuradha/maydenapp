<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ShoppingListController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function () {
  Route::get('/', [ShoppingListController::class, 'index'])->name('shopping.index');
  Route::get('/shopping/add-item', [ShoppingListController::class, 'create'])->name('shopping.create');
  Route::post('/shopping', [ShoppingListController::class, 'store'])->name('shopping.store');
  Route::delete('/shopping/{id}', [ShoppingListController::class, 'destroy'])->name('shopping.destroy');
  Route::post('/shopping/reorder', [ShoppingListController::class, 'reorder'])->name('shopping.reorder');
  Route::put('/shopping/{id}/picked', [ShoppingListController::class, 'picked'])->name('shopping.picked');
  Route::get('/shopping/share', [ShoppingListController::class, 'showShareForm'])->name('shopping.shareform');
  Route::post('/shopping/share', [ShoppingListController::class, 'share'])->name('shopping.share');

});


