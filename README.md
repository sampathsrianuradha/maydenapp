## Shopping List Application

## Installation

1. Clone the repository to your local machine.

2. Install the necessary dependencies and copy the .env.example file to .env.
   composer install & npm install

3. Create a new database and update the database settings in the .env file.

4. Run the database migrations.
   php artisan migrate

5. Start the local web server.
    php artisan serve

6. Configure smtp to work with email


## Usage

Once the application is up and running, you can access it by visiting http://localhost:8000


## Features

### Creating Items

To create a new shopping item, click on the "Add Item" button and enter the name of the item in the form that appears.

### Marking Shopping Items as Picked

To mark an item as picked, simply click on the checkbox next to the item name.

### Reordering Items

To reorder items, simply drag and drop them to the desired position.

### Share the List via email

To share the list via email, click on the "Share via Email" button and enter the recipient's email address.
