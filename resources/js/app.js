import './bootstrap';

$(document).ready(function() {
    let csrftoken = $('meta[name=csrf-token]').attr('content');

    $('#sortable').sortable({
        axis: 'y',
        update: function (event, ui) {
            let ids = $(this).sortable('toArray');
            // var ids = $(this).sortable('serialize');
            console.log(ids);
            let data = {
                _token: csrftoken,
                ids: ids
            };

            $.ajax({
                url: 'shopping/reorder',
                type: 'POST',
                data: data,
                success: function (response) {
                    console.log(response);
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
    });
});
