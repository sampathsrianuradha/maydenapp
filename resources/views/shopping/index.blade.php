@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Shopping List') }}</div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if(count($shoppingItems) > 0)
                      <ul id="sortable" class="list-group">
                        @foreach($shoppingItems as $item)
                          <li class="list-group-item d-flex" id="{{ $item->id }}">
                            <div class="flex-grow-1">
                            {{ $item->name }}
                            </div>
                            <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                              <form action="{{ route('shopping.destroy', $item->id) }}" method="POST" style="display: inline">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to delete this item?')">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-square-fill" viewBox="0 0 16 16">
                                      <path d="M2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2zm3.354 4.646L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 1 1 .708-.708z"/>
                                    </svg>
                                  </button>
                              </form>
                              @if(!$item->picked)
                              <form action="{{ route('shopping.picked', $item->id) }}" method="POST" style="display: inline">
                                  @csrf
                                  @method('PUT')
                                  <button type="submit" class="btn btn-sm btn-warning" >
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-cart" viewBox="0 0 16 16">
                                      <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
                                    </svg>
                                  </button>
                              </form>
                              @else
                                <span class="badge bg-secondary pt-2">PICKED UP</span>
                              @endif
                            </div>
                          </li>
                        @endforeach
                      </ul>
                    @else
                      <p>Your shopping list is empty.</p>
                    @endif

                </div>
                <div class="card-footer">
                  <a href="{{ route('shopping.shareform') }}" class="btn btn-sm btn-light" >Share via Email</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
