{{ 'My shopping list:' }}

<ul>
    @foreach ($listItems as $item)
        <li>{{ $item }}</li>
    @endforeach
</ul>
