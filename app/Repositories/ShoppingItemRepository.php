<?php

namespace App\Repositories;

use App\Models\ShoppingItem;
use \Illuminate\Database\Eloquent\Collection;
class ShoppingItemRepository
{
    /**
     * Get all shopping items for a user.
     *
     * @param int $userId The ID of the user.
     * @return \Illuminate\Database\Eloquent\Collection A collection of shopping items.
     */
    public function getAllForUser($userId): Collection
    {
        return ShoppingItem::where('user_id', $userId)->orderBy('order')->get();
    }

   /**
     * Find a shopping item by ID.
     *
     * @param int $id The ID of the shopping item.
     * @return \App\Models\ShoppingItem|null The shopping item, or null if not found.
     */
    public function find($id): ShoppingItem
    {
        return ShoppingItem::find($id);
    }

    /**
     * Create a new item.
     *
     * @param array $data The data for the shopping item.
     * @return \App\Models\ShoppingItem Newly created shopping item.
     */
    public function create($data): ShoppingItem
    {
        $shoppingItem = new ShoppingItem();
        $shoppingItem->name = $data['name'];
        $shoppingItem->user_id = $data['user_id'];
        $shoppingItem->order = $this->getNextOrderId($data['user_id']);
        $shoppingItem->save();

        return $shoppingItem;
    }

    /**
     * Update the picked or unpicked status
     *
     * @param int $id The ID of the shopping item.
     * @return void
     */
    public function updatePicked($id) : void
    {
        $shoppingItem = ShoppingItem::find($id);
        $shoppingItem->picked = !!!$shoppingItem->picked;
        $shoppingItem->save();
    }

    /**
     * Delete a shopping item by ID.
     *
     * @param int $id The ID of the shopping item.
     * @return void
     */
    public function delete($id): void
    {
        $shoppingItem = ShoppingItem::find($id);
        $shoppingItem->delete();
    }

    /**
     * update the order of the shopping items.
     *
     * @param array $ids list of IDs the shopping items.
     * @return void
     */
    public function updateOrder($ids): void
    {
        foreach ($ids as $order => $id) {
            ShoppingItem::where('id', $id)->update(['order' => $order]);
        }
    }

    /**
     * get the next order id.
     *
     * @param int $userId The ID of the user.
     * @return void
     */
    public function getNextOrderId($userId): int
    {
        return ShoppingItem::where('user_id', $userId)->max('order') + 1;
    }

    /**
     * Get all picked shopping items for a user.
     *
     * @param int $userId The ID of the user.
     * @param bool $picked Whether to get picked or unpicked shopping items.
     * @return \Illuminate\Database\Eloquent\Collection A collection of shopping items.
     */
    public function getByUserIdAndPicked($userId, $picked = true): Collection
    {
        return ShoppingItem::where('user_id', $userId)->where('picked', $picked)->get();
    }

    /**
     * TODO
     * Get total price of the shopping list items..
     *
     * @param int $userId The ID of the user.
     * @return decimal Total price of the shopping list items.
     */
    public function totalPrice(): decimal 
    {
      //did not work on due to API not responding
      //return the total price of the shopping items
    }

}
