<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ShoppingItemRepository;
use App\Mail\SharedShoppingList;
use Illuminate\Support\Facades\Mail;
use App\Exceptions\EmailException;


class ShoppingListController extends Controller
{
    protected $shoppingItemRepository;

    /**
     * constructor 
     *
     * @param  ShoppingItemRepository  $shoppingItemRepository
     * @return void
     */
    public function __construct(ShoppingItemRepository $shoppingItemRepository)
    {
        $this->middleware('auth');
        $this->shoppingItemRepository = $shoppingItemRepository;
    }

    /**
     * Display a listing of shopping items.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();
        $shoppingItems = $this->shoppingItemRepository->getAllForUser($user->id);
        return view('shopping.index', compact('shoppingItems'));
    }

    /**
     * Show add item form.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('shopping.create');
    }

    /**
     * Store a shopping item.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255'
        ]);

        $shoppingItem = $this->shoppingItemRepository->create([
            'name' => $request->input('name'),
            'user_id' => auth()->id(),
        ]);

        return redirect()->route('shopping.index')->with('success', 'Item added to shopping list!');
    }

    /**
     * Update an item as picked or unpicked.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function picked($id)
    {
        $this->shoppingItemRepository->updatePicked($id);

        return redirect()->route('shopping.index');
    }

    /**
     * Remove the specified item from shopping list.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->shoppingItemRepository->delete($id);
        return redirect()->route('shopping.index')->with('success', 'Item removed from shopping list!');
    }

    /**
     * Reorder the shopping list.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reorder(Request $request)
    {
        $ids = $request->ids;
        $this->shoppingItemRepository->updateOrder($ids);

        return response()->json(['success' => true]);
    }


    /**
     * Show the form for sharing the shopping list.
     *
     * @return \Illuminate\View\View
     */
    public function showShareForm()
    {
        return view('shopping.share');
    }

    /**
     * Share the shopping list via email.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Exceptions\EmailException
     */
    public function share(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
        ]);

        $user = auth()->user();
        $listItems = $this->shoppingItemRepository->getByUserIdAndPicked($user->id);

        $data = [
            'listItems' => $listItems
        ];

        try {
          Mail::to($request->input('email'))->send(new SharedShoppingList($data));
        } catch (\Exception $e) {
            throw new EmailException('Error sending email', 500, $e);
        }

        return redirect()->back()->with('success', 'Shopping list shared successfully');
    }
}
